package cl.demo.exchangerate.service.impl;

import java.io.FileReader;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.demo.exchangerate.dto.ExchangeRateResponse;
import cl.demo.exchangerate.feign.client.ExchangeRateFeignClient;
import cl.demo.exchangerate.service.ExchangeRateService;

@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

	@Autowired
	ExchangeRateFeignClient erfc;

	@Override
	public ExchangeRateResponse getExchangeRate() {
		ExchangeRateResponse response = new ExchangeRateResponse();

		try (FileReader reader = new FileReader(getCurrencyByCountryUrlJSON())) {
			String countryCode = this.getCountryCode();
			JSONParser jparser = new JSONParser();
			JSONObject data = (JSONObject) jparser.parse(reader);
			String currency = (String) data.get(countryCode);

			String erfr = erfc.getExchangeRate().getBody();
			JSONObject exchangeRates = new JSONObject();
			exchangeRates = (JSONObject) jparser.parse(erfr);
			JSONObject rates = (JSONObject) exchangeRates.get("rates");
			response.setBase(exchangeRates.get("base").toString());
			response.setTo(currency);
			response.setCountryCode(countryCode);
			if (currency != null) {
				response.setValue(Double.parseDouble(rates.get(currency).toString()));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	private String getCountryCode() {
		Map<String, String> env = System.getenv();
		Set<String> keys = env.keySet();
		for (String key : keys) {
			if (key.equals("COUNTRY_CODE")) {
				System.out.println(key + " = " + env.get(key));
				return env.get("COUNTRY_CODE");
			}
		}
		return null;

	}

	private String getCurrencyByCountryUrlJSON() {
		Map<String, String> env = System.getenv();
		Set<String> keys = env.keySet();
		for (String key : keys) {
			if (key.equals("PATH_ISO_CURRENCY")) {
				System.out.println(key + " = " + env.get(key));
				return env.get("PATH_ISO_CURRENCY");
			}
		}
		return null;
	}

}
