package cl.demo.exchangerate.service;

import cl.demo.exchangerate.dto.ExchangeRateResponse;

public interface ExchangeRateService {

	public ExchangeRateResponse getExchangeRate();
}
