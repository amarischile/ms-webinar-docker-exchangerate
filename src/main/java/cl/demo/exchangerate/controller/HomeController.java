package cl.demo.exchangerate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.demo.exchangerate.dto.ExchangeRateResponse;
import cl.demo.exchangerate.service.ExchangeRateService;

@RestController
public class HomeController {

	@Autowired
	ExchangeRateService ers;

	@CrossOrigin(origins = "*")
	@GetMapping("/")
	public ResponseEntity<ExchangeRateResponse> getExchangeRate() {
		ExchangeRateResponse response = ers.getExchangeRate();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
