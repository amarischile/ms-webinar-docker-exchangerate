package cl.demo.exchangerate.dto;

public class ExchangeRateResponse {

	private String base;
	private String to;
	private Double value;
	private String countryCode;

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public ExchangeRateResponse() {
		super();
	}

	public String getCountryCode() {
		return countryCode;
	}

	public ExchangeRateResponse(String base, String to, Double value, String countryCode) {
		super();
		this.base = base;
		this.to = to;
		this.value = value;
		this.countryCode = countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
