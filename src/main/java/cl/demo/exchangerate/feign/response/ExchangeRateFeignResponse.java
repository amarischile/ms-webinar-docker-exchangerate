package cl.demo.exchangerate.feign.response;

public class ExchangeRateFeignResponse {

	private String base;
	private String rates;

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getRates() {
		return rates;
	}

	public void setRates(String rates) {
		this.rates = rates;
	}

	public ExchangeRateFeignResponse(String base, String rates) {
		super();
		this.base = base;
		this.rates = rates;
	}

	public ExchangeRateFeignResponse() {
		super();
	}

}
