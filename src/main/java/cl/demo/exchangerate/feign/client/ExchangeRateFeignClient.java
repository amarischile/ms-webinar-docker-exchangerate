package cl.demo.exchangerate.feign.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "EXCHANGE-RATE", url = "https://openexchangerates.org/api")
public interface ExchangeRateFeignClient {

	@GetMapping(value = "/latest.json?app_id=cfe2209ff4994918929ef7b56b98efe7")
	public ResponseEntity<String> getExchangeRate();
}
