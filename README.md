# MS EXCHANGE-RATE

Micro servicio Exchange-Rate, encargado de obtener el tipo de cambio por pais.
Para su construccion se utiliza un `Dockerfile` multistage, la imagen base de la primera etapa es `openjdk:8-jdk-alpine` la cual genera el binario compilado del microservicio y para 
la etapa de ejecucion se utiliza imagen base `openjdk:8-jre-alpine`.

## Container Configuration

### BUILD

Para la contruccion del contenedor es necesario ejecutar el siguiente comando:

```sh
docker build -t webinar-docker/ms-exchange-rate .
```

Flag `-t` es utilizado para crear un tag de la imagen

### ENV
Variables de entorno necesarias para la correcta ejecucion del contenedor

```sh
TZ=America/Santiago
COUNTRY_CODE=CL 
PATH_ISO_CURRENCY=/app/isoCurrencyByCountry.json  
```

* TZ: Define zona horaria (America/Santiago)
* COUNTRY_CODE: Pais del que se quiere obtener el tipo de cambio
* PATH_ISO_CURRENCY: Archivo utilizado para obtener el codigo ISO del Currency por el pais dado

### PORT FORWARDING

Contenedor se instanciará en el puerto `9000`

```sh
docker run -p 9000:8080 ... ... ...
```

Se le indica que todo el trafico in/out por el puerto `9000` del host hacia el `9000` del contenedor.

## Container Execution

Para la ejecucion solo hace falta tener instalado Docker dentro de la maquina y en una consola ejecutar lo siguiente:

```sh
docker build -t webinar-docker/ms-exchange-rate .
```

Con esto crearemos la imagen llamada `webinar-docker/ms-exchange-rate` y con el `.` le decimos que contruya en base al Dockerfile del directorio.

Luego ejecutamos lo siguiente para instanciar el contenedor.

```sh
docker run --rm -p 9000:8080 \
    --name ms-webinar-docker-exchange-rate \
    --network webinar-docker \
    -e TZ=America/Santiago \
    -e COUNTRY_CODE=CL \
    -e PATH_ISO_CURRENCY=/app/isoCurrencyByCountry.json \
    webinar-docker/ms-exchange-rate
```

Esto ejecutará el contenedor de Docker en el puerto `9000` y le añade multiples variables de entorno y configuraciones.
En caso de querer ejecutar la imagen en background agregar luego del `-p 9000:8080` un `-d` o `--detached`.