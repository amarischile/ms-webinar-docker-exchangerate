FROM openjdk:8-jdk-alpine as build
WORKDIR /workspace/app

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src
COPY src/main/resources/isoCurrencyByCoutry.json ./

RUN ./mvnw install -DskipTests

FROM openjdk:8-jdk-alpine

VOLUME /tmp

ARG APP_NAME
ARG DEPENDENCY=/workspace/app/target
ARG ARTIFACT_NAME
ENV APP_NAME=$APP_NAME

COPY --from=build /workspace/app/isoCurrencyByCoutry.json /app/isoCurrencyByCountry.json 
COPY --from=build /workspace/app/target/exchange-rate-1.0.0.jar /app/exchange-rate.jar

ENTRYPOINT ["java","-jar","/app/exchange-rate.jar"]